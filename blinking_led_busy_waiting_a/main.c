/**
 *******************************************************************************
 * @file		main.c
 *
 * @brief		Blinking LED, busy waiting
 * 
 * @author		Sebastien Masson
 *
 * @date		July 28th 2018
 *
 * @version		1.0
 *
 *******************************************************************************
 * @details		Busy waiting example to turn a LED (active low) on and off.
 *				Target: LPCXpresso845-MAX Board (LPC845)
 *
 * @see			https://www.nxp.com/support/developer-resources/hardware-development-tools/lpcxpresso-boards/lpcxpresso845-max-board-for-lpc84x-mcu-family:OM13097
 *******************************************************************************
 * @mainpage	Project (1): "blinking_led_busy_waiting"
 * @section 	sec_intro	Description
 *				Busy waiting example to turn a LED (active low) on and off.
 *				Target: LPCXpresso845-MAX Board (LPC845)
 */
#include <LPC845.h>

/**
 * @def DELAY
 * Busy waiting delay
 */
#define DELAY	1000000


void delay_bw(unsigned int t);	// Busy waiting function

/**
 * @brief	Program entry point
 */
int main(){
	/*
	 * Peripheral reset: GPIO, Port 0 (Not necessary)
	 */
	//SYSCON_PRESETCTRL0_GPIO0_RST_N(0);	// Peripheral reset: GPIO_0
	//SYSCON_PRESETCTRL0_GPIO0_RST_N(1);	// Release reset
	
	/*
	 * Enable CLK for GPIO, port 0
	 */
	//SYSCON->SYSAHBCLKCTRL0 |= (1u << 6);						// Method 1
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO0(1);	// Method 2
	
	/*
	 * Configure & Set GPIO direction: P0.0
	 */
	GPIO->DIR[0] |= (1u << 0);			// Configure GPIO direction
	GPIO->DIRSET[0] |= (1u << 0);		// Set GPIO direction
	//GPIO->DIR[1] = (1u << x);			// Example for P1.x: Configure
	//GPIO->DIRSET[1] |= (1u << x);		//					 Set
	
	/*
	 * Toggle pin
	 */
	GPIO->CLR[0] |= (1u << 0);			// Clear P0.0 (active low)
	//GPIO->SET[0] |= (1u << 0);		// Set P0.0	(active high)
	//GPIO->SET[1] |= (1u << x);		// Example for P1.x (active high)
	
	/*
	 * Busy waiting blinking output
	*/
	while(1){
		GPIO->SET[0] |= (1u << 0);		// Set P0.0 --> LED is off
		delay_bw(DELAY);
		
		GPIO->CLR[0] |= (1u << 0);		// Clear P0.0 --> LED is on
		delay_bw(DELAY);
	}
	
}

/**
 * @brief	Busy waiting delay function
 *
 * @code	delay_bw(1000000);
 * @endcode
 *
 * @param	t	Number of loop iterations lost in busy waiting
 */
void delay_bw(unsigned int t)
{
	unsigned int i;
	for(i=0; i<t; i++);
}
