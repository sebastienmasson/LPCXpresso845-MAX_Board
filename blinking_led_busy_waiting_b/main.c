/**
 *******************************************************************************
 * @file		main.c
 *
 * @brief		Blinking LED, busy waiting
 * 
 * @author		Sebastien Masson
 *
 * @date		July 28th 2018
 *
 * @version		1.0
 *
 *******************************************************************************
 * @details		Busy waiting example to turn a LED (active low) on and off.
 *				Includes user-defines functions and macros.
 *				Target: LPCXpresso845-MAX Board (LPC845)
 *
 * @see			https://www.nxp.com/support/developer-resources/hardware-development-tools/lpcxpresso-boards/lpcxpresso845-max-board-for-lpc84x-mcu-family:OM13097
 *******************************************************************************
 * @mainpage	Project (1): "blinking_led_busy_waiting"
 * @section 	sec_intro	Description
 *				Busy waiting example to turn a LED (active low) on and off.
 *				Target: LPCXpresso845-MAX Board (LPC845)
 */
#include <LPC845.h>
#include <assert.h>

/**
 * @def DELAY
 * Busy waiting delay
 */
#define DELAY	1000000

#define GREENLED_PORT	0	// Green LED is P0.0
#define GREENLED_PIN	0

#define BLUELED_PORT	1	// Blue LED is P1.15
#define BLUELED_PIN		15

/******************************************************************************/

void delay_bw(unsigned int t);					// Busy waiting function
void LED_init(uint32_t port, uint32_t pin);		// Init GPIO and pin direction
void LED_on(uint32_t port, uint32_t pin);		// Turn output on
void LED_off(uint32_t port, uint32_t pin);		// Turn output off
void LED_toggle(uint32_t port, uint32_t pin);	// Toggle output


/******************************************************************************/

/**
 * @brief	Program entry point
 */
int main()
{
	unsigned int i;
	
	LED_init(GREENLED_PORT, GREENLED_PIN);
	LED_init(BLUELED_PORT, BLUELED_PIN);
	
	LED_on(GREENLED_PORT, GREENLED_PIN);
	LED_off(BLUELED_PORT, BLUELED_PIN);
	while(1)
	{
		delay_bw(DELAY);
		LED_toggle(GREENLED_PORT, GREENLED_PIN);
		LED_toggle(BLUELED_PORT, BLUELED_PIN);
	}
}

/******************************************************************************/


/**
 * @brief	Busy waiting delay function
 *
 * @code	delay_bw(1000000);
 * @endcode
 *
 * @param	t	Number of loop iterations lost in busy waiting
 */
void delay_bw(unsigned int t)
{
	unsigned int i;
	for(i=0; i<t; i++);
}

/**
 * @brief	Enable the GPIO CLK and set the pin direction
 *
 * @code	LED_init(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_init(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	if (port == 0)
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO0(1);	// CLK GPIO0
	else
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO1(1);	// CLK GPIO1
	
	GPIO->DIR[port] |= GPIO_DIR_DIRP(1u << pin);		// Configure direction
	GPIO->DIRSET[port] |= GPIO_DIRSET_DIRSETP(1u << pin);
}

/**
 * @brief	Switch the LED on (ties it to ground)
 *
 * @code	LED_on(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_on(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->CLR[port] |= GPIO_CLR_CLRP(1u << pin);		// Turn the LED on
}

/**
 * @brief	Switch the LED off (ties it to Vcc)
 *
 * @code	LED_off(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_off(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->SET[port] |= GPIO_SET_SETP(1u << pin);		// Turn the LED off
}

/**
 * @brief	Toggle the LED
 *
 * @code	LED_toggle(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_toggle(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->NOT[port] |= GPIO_NOT_NOTP(1u << pin);		// Toggle the LED
}
