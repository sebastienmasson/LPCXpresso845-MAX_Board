# Getting started with Cortex-M0
Projets into this directory all have been ran properly using the LPCXpresso845-MAX Board.

## Projects Listing

### Basics of GPIO
Progression | Folder | Description
--- | --- | ---
(1) | `blinking_led_busy_waiting_a` | Toggles P0.0 and bussy waits in the meantime (using LPC definitions and macros)
(2) | `blinking_led_busy_waiting_b` | Toggles P0.0 and bussy waits in the meantime (using custom functions)
(3) | `blinking_led_interrupt` | Toggles P0.0, interrupt driven
(4) | `push_toggle_busy_waiting` | Toggles P1.15 on key pressed, polling
(5) | `push_toggle_interrupt` | Toggles P1.15 on key pressed, interrupt driven
(6) | `push_pwm` | Increase and decrease PWM duty cycle by pressing a key or another
(7) | `lcd_helloworld` | Write down "Hello world" on a 16x2 (HD44780) liquid cristal display
(x) | `i2c_poc` | Read temperature from an I2C sensor
