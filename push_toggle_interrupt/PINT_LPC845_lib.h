/**
********************************************************************************
* @file			PINT_LPC845_lib.h
*
* @brief		Generic functions for working with GPIO
*
* @author		Sebastien Masson
*
* @date			July 29th 2018
*
* @version		N/A (Deprecated - Built as the projects progress)
********************************************************************************
* @verbatim		How to use this library?
*				========================
*				Public functions all are listed in PINT_LPC845.h where they are
*				sorted on their purposes.
*				Functions documentation is kept in PINT_LPC845.c.
* @endverbatim
********************************************************************************
*/

#ifndef __PINT_LPC845_LIB__
#define __PINT_LPC845_LIB__

#include <LPC845.h>
#include <assert.h>

/**
 * @addtogroup pint_modes PIN MODES
 * @{
 */
#define PINT_EDGE_RISING	1		/**< Interrupt on rising edge */
#define PINT_EDGE_FALLING	2		/**< Interrupt on falling edge */
#define PINT_EDGE_BOTH		3		/**< Interrupt on both edges */
#define PINT_ACTIVE_HIGH	4		/**< Interrupt when pin is high */
#define PINT_ACTIVE_LOW		5		/**< Interrupt when pin is low */
/*!
 * @}
 */ /* end of pin_modes */

/**
 * @addtogroup pint_id PIN INTERRUPTION IDENTIFICATION NUMBERS
 * @{
 */
#define PINT0	0		/**< Pin Interruption #0 */
#define PINT1	1		/**< Pin Interruption #1 */
#define PINT2	2		/**< Pin Interruption #2 */
#define PINT3	3		/**< Pin Interruption #3 */
#define PINT4	4		/**< Pin Interruption #4 */
#define PINT5	5		/**< Pin Interruption #5 */
#define PINT6	6		/**< Pin Interruption #6 */
#define PINT7	7		/**< Pin Interruption #7 */
/*!
 * @}
 */ /* end of pint_id */

/***** INIT *******************************************************************/
void pint_init(uint32_t port, uint32_t pin, uint8_t id);

/***** CONFIG *****************************************************************/
void pint_config(uint8_t pintn, uint8_t pintmode);

/***** MANAGEMENT *************************************************************/
void pint_clear(uint8_t pintn);

#endif
