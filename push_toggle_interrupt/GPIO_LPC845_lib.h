/**
********************************************************************************
* @file			GPIO_LPC845_lib.h
*
* @brief		Generic functions for working with GPIO
*
* @author		Sebastien Masson
*
* @date			July 29th 2018
*
* @version		N/A (Deprecated - Built as the projects progress)
********************************************************************************
* @verbatim		How to use this library?
*				========================
*				Public functions all are listed in GPIO_LPC845.h where they are
*				sorted on their purposes.
*				Functions documentation is kept in GPIO_LPC845.c.
* @endverbatim
********************************************************************************
*/
#ifndef __GPIO_LPC845_LIB__
#define __GPIO_LPC845_LIB__

#include <LPC845.h>
#include <assert.h>
#include <stdbool.h>

/***** INIT *******************************************************************/
void output_init(uint32_t port, uint32_t pin);
void input_init(uint32_t port, uint32_t pin);

/***** INPUT ******************************************************************/
uint8_t input1_read8(uint32_t port, uint32_t pin);

/***** OUTPUT *****************************************************************/
void output1_set1(uint32_t port, uint32_t pin);
void output1_clear1(uint32_t port, uint32_t pin);
void output1_toggle1(uint32_t port, uint32_t pin);

#endif
