/**
********************************************************************************
* @file			PINT_LPC845_lib.c
*
* @brief		Generic functions for working with GPIO
*
* @author		Sebastien Masson
*
* @date			July 29th 2018
*
* @version		N/A (Deprecated - Built as the projects progress)
********************************************************************************
* @details		This library offers a simplified interface for working with pin
*				interruptions without the need to deal with registers.
*
* @verbatim		Conventions used in this file
*				=============================
*				Function names
*				--------------
*				(1) All public functions starts with "pint_" which stands for
*				    "pin interruption"
*
*				How to use this library?
*				========================
*				Public functions all are listed in PINT_LPC845.h where they are
*				sorted on their purposes.
*				Functions documentation is kept in PINT_LPC845.c.
*				
*				PINT_LPC845.h also contains usefull definitions that help to
*				keep playing with registers at bay:
*				- PIN MODES: Define how the configured interrupt behave;
*				- PIN INTERRUPTION IDENTIFICATION NUMBERS: Help to identify the
*				  eight pin interruptions
*				
*				Interrupt handlers
*				==================
*				Interrupt handlers are defined into the StartUp file.
*				
*				Important reminder
*				==================
*				Do not forget to clear the interrupt in the interrupt handler
* @endverbatim
********************************************************************************
*/

#include "PINT_LPC845_lib.h"

/*
================================================================================
===== INITIALISATION FUNCTIONS =================================================
================================================================================
*/

/**
 * @brief	Initialise pin interruption
 *
 * @code	pint_init(1,15,PINT0);
 * @endcode
 *
 * @param	port		Output port number
 *			pin			Output pin number
 *			pintn		PinInt IDENTIFICATION NUMER (defined into PINT_LPC845.h)
 */
void pint_init(uint32_t port, uint32_t pin, uint8_t pintn)
{
	IRQn_Type IRQn;
	
	assert(port <= 1);
	assert(pin <= 31);
	assert(pintn <= 7);
	
	/* (1) Select pin issuing the interrupt */
	SYSCON->PINTSEL[pintn] = SYSCON_PINTSEL_INTPIN((port*32)+pin);

	/* (2) Enable CLK for pin interruptions */
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO_INT(1);

	/* (3) Enable pin interruptions into NVIC */
	switch(pintn)							// Align pintn and IRQn
	{
		case 0:
			IRQn = PIN_INT0_IRQn;
		break;
		case 1:
			IRQn = PIN_INT1_IRQn;
		break;
		case 2:
			IRQn = PIN_INT2_IRQn;
		break;
		case 3:
			IRQn = PIN_INT3_IRQn;
		break;
		case 4:
			IRQn = PIN_INT4_IRQn;
		break;
		case 5:
			IRQn = PIN_INT5_DAC1_IRQn;
		break;
		case 6:
			IRQn = PIN_INT6_USART3_IRQn;
		break;
		case 7:
			IRQn = PIN_INT7_USART4_IRQn;
		break;
	}
	NVIC_EnableIRQ(IRQn);					// Enable PinInt into NVIC
}

/*
================================================================================
===== CONFIGURATION FUNCTIONS ==================================================
================================================================================
*/

/**
 * @brief	Configure the pin interruption given in argument
 *
 * @code	pint_config(PINT0,PINT_EDGE_FALLING);
 * @endcode
 *
 * @param	pintn		PinInt IDENTIFICATION NUMER (defined into PINT_LPC845.h)
 *			pintmode	One of the PIN MODES defined in PINT_LPC845.h
 */
void pint_config(uint8_t pintn, uint8_t pintmode)
{	
	assert(pintn <= 7);
	
	switch(pintmode)
	{
		case PINT_EDGE_RISING:
			PINT->ISEL |= PINT_ISEL_PMODE(~(1u << pintn));	// Mode: Edge (0)
			PINT->SIENR |= PINT_SIENR_SETENRL(1u << pintn);	// Trig: Rising edge
			PINT->CIENF |= PINT_CIENF_CENAF(1u << pintn);
		break;
		case PINT_EDGE_FALLING:
			PINT->ISEL |= PINT_ISEL_PMODE(~(1u << pintn));	// Mode: Edge (0)
			PINT->CIENR |= PINT_CIENR_CENRL(1u << pintn);
			PINT->SIENF |= PINT_SIENF_SETENAF(1u << pintn);	// Trig: Falling edge
		break;
		case PINT_EDGE_BOTH:
			PINT->ISEL |= PINT_ISEL_PMODE(~(1u << pintn));	// Mode: Edge (0)
			PINT->SIENR |= PINT_SIENR_SETENRL(1u << pintn);	// Trig: Rising edge
			PINT->SIENF |= PINT_SIENF_SETENAF(1u << pintn);	// Trig: Falling edge
		break;
		case PINT_ACTIVE_HIGH:
			PINT->ISEL |= PINT_ISEL_PMODE(1u << 0);			// Mode: Level (1)
			PINT->SIENR |= PINT_SIENR_SETENRL(1u << pintn);	// Enable level
			PINT->SIENF |= PINT_SIENF_SETENAF(1u << pintn);	// Active high
		break;
		case PINT_ACTIVE_LOW:
			PINT->ISEL |= PINT_ISEL_PMODE(1u << 0);			// Mode: Level (1)
			PINT->SIENR |= PINT_SIENR_SETENRL(1u << pintn);	// Enable level
			PINT->CIENF |= PINT_CIENF_CENAF(1u << pintn);	// Active low
		break;
		default:	// PINT_EDGE_RISING
			PINT->ISEL |= PINT_ISEL_PMODE(~(1u << pintn));	// Mode: Edge (0)
			PINT->SIENR |= PINT_SIENR_SETENRL(1u << pintn);	// Trig: Rising edge
			PINT->CIENF |= PINT_CIENF_CENAF(1u << pintn);
		break;
	}
	
}

/*
================================================================================
===== PIN INTERRUPTS MANAGEMENT ================================================
================================================================================
*/

/**
 * @brief	Clear the pin interruption flag given in argument
 *
 * @code	pint_clear(PINT0);
 * @endcode
 *
 * @param	pintn		PinInt IDENTIFICATION NUMER (defined into PINT_LPC845.h)
 *			pintmode	One of the PIN MODES defined in PINT_LPC845.h
 */
 void pint_clear(uint8_t pintn)
{
	PINT->IST |= PINT_IST_PSTAT(1u << pintn);
}

