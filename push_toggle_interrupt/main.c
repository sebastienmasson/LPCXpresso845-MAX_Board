/**
********************************************************************************
* @file			main.c
*
* @brief		Toggle LED when button state changes
*
* @author		Sebastien Masson
*
* @date			July 29th 2018
*
* @version		1.0
********************************************************************************
* @details		Example program toggles a LED on button pressed, falling edge 
*				Target: LPCXpresso845-MAX Board (LPC845)
*
* @see			https://www.nxp.com/support/developer-resources/hardware-development-tools/lpcxpresso-boards/lpcxpresso845-max-board-for-lpc84x-mcu-family:OM13097
********************************************************************************
* @mainpage	Project (5): "push_toggle_interrupt"
* @section 	sec_intro	Description
*				Example program switches an output each time a button is pressed
*				Target: LPCXpresso845-MAX Board (LPC845)
*/
#include "main.h"

void PIN_INT0_IRQHandler(void);

/*******************************************************************************
* @brief Program entry point
*******************************************************************************/
int main()
{
	// I/O initialisation
	LED_INIT(BLUELED_PORT,BLUELED_PIN);
	BUT_INIT(BUTTON_PORT,BUTTON_PIN);
	
	// Pin Interrupt initialisation
	pint_init(BUTTON_PORT,BUTTON_PIN,PINT0);	// Set pin int #0
	pint_config(PINT0,PINT_EDGE_FALLING);		// Configure pin int #0
	
	LED_OFF(BLUELED_PORT,BLUELED_PIN);
	
	while(1)
	{
		/* Insert some pieces of code here */
	}
}

/*******************************************************************************
* @brief Pin interruption handler #0
*******************************************************************************/
void PIN_INT0_IRQHandler(void)
{
	LED_TOGGLE(BLUELED_PORT,BLUELED_PIN);
	pint_clear(PINT0);							// Clear interrupt flag #0
}
