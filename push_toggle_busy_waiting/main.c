/**
********************************************************************************
* @file			main.c
*
* @brief		Toggle LED when button state changes
*
* @author		Sebastien Masson
*
* @date			July 29th 2018
*
* @version		1.0
********************************************************************************
* @details		Example program toggles a LED on button pressed, falling edge 
*				Target: LPCXpresso845-MAX Board (LPC845)
*
* @see			https://www.nxp.com/support/developer-resources/hardware-development-tools/lpcxpresso-boards/lpcxpresso845-max-board-for-lpc84x-mcu-family:OM13097
********************************************************************************
* @mainpage	Project (4): "push_toggle_busy_waiting"
* @section 	sec_intro	Description
*				Example program switches an output each time a button is pressed
*				Target: LPCXpresso845-MAX Board (LPC845)
*/
#include "main.h"

/*******************************************************************************
* @brief Program entry point
*******************************************************************************/
int main()
{
	volatile uint8_t buttonvalue;
	uint8_t buttonformervalue;
	
	LED_INIT(BLUELED_PORT,BLUELED_PIN);
	BUT_INIT(BUTTON_PORT,BUTTON_PIN);
	
	while(1)
	{
		buttonvalue = KEYPRESSED(BUTTON_PORT,BUTTON_PIN);
		if (buttonvalue != buttonformervalue)	// Only if state has changed
		{
			buttonformervalue = buttonvalue;
			if (buttonvalue == 0)				// Button is pressed
				LED_TOGGLE(BLUELED_PORT,BLUELED_PIN);
		}
	}
}
