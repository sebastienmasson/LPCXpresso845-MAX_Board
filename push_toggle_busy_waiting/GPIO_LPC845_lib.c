/**
********************************************************************************
* @file			GPIO_LPC845_lib.c
*
* @brief		Generic functions for working with GPIO
*
* @author		Sebastien Masson
*
* @date			July 29th 2018
*
* @version		N/A (Deprecated - Built as the projects progress)
********************************************************************************
* @details		This library offers a simplified interface for working with GPIO
*				without the need to deal with registers.
*
* @verbatim		Conventions used in this file
*				=============================
*				Function names
*				--------------
*				(1) Initialisation functions: end with suffix "_init"
*				(2) Input and Output functions: 
*					- Prefix    : "input" or "output"
*					- Bit amount: to be read/written on the chip pin
*					- Suffix    : Role of the function ("read", "write", "set",
*								  or "clear")
*					- Bit amount: to be read/written into software
*				
*				Example : "input1_read8()"
*					- Input function; reads 1 bit from a microcontroller pin
*					- Returns the read value as a byte
*				
*				Recommended usage
*				-----------------
*				Use macro in your source code to increase readability.
*				Example:	#define LED_PORT	1
*							#define LED_PIN		12
*							#define LED_ON(t,n)	output1_set1(t,n);
*
*				How to use this library?
*				========================
*				Public functions all are listed in GPIO_LPC845.h where they are
*				sorted on their purposes.
*				Functions documentation is kept in GPIO_LPC845.c.
* @endverbatim
********************************************************************************
*/
#include "GPIO_LPC845_lib.h"

/*
================================================================================
===== INITIALISATION FUNCTIONS =================================================
================================================================================
*/
/**
 * @brief	Enable the GPIO CLK and set the pin direction to output
 *
 * @code	output_init(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void output_init(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	if (port == 0)
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO0(1);	// CLK GPIO0
	else
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO1(1);	// CLK GPIO1
	
	GPIO->DIRSET[port] |= GPIO_DIRSET_DIRSETP(1u << pin);	// Output direction
}


/**
 * @brief	Enable the GPIO CLK and set the pin direction to input
 *
 * @code	input_init(1,15);
 * @endcode
 *
 * @param	port	Input port number
 *			pin		Input pin number
 */
void input_init(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	if (port == 0)
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO0(1);	// CLK GPIO0
	else
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO1(1);	// CLK GPIO1
	
	GPIO->DIRCLR[port] = GPIO_DIRCLR_DIRCLRP(1u << pin);	// Input direction
}

/*
================================================================================
===== INPUTS READING FUNCTIONS =================================================
================================================================================
*/

/**
 * @brief	Reads one input, returns one byt
 *
 * @code	value = input1_read8(1,15);
 * @endcode
 *
 * @param	port	Input port number
 *			pin		Input pin number
 */
uint8_t input1_read8(uint32_t port, uint32_t pin)
{
	return GPIO->B[port][pin];
}

/*
================================================================================
===== OUTPUTS WRITING FUNCTIONS ================================================
================================================================================
*/

/**
 * @brief	Set one output pin
 *
 * @code	output1_set1(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void output1_set1(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->SET[port] = GPIO_SET_SETP(1u << pin);
}

/**
 * @brief	Clear one output pin
 *
 * @code	output1_clear1(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void output1_clear1(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->CLR[port] =GPIO_CLR_CLRP(1u << pin);
}

/**
 * @brief	Toggle one output pin
 *
 * @code	output1_toggle1(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void output1_toggle1(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->NOT[port] = GPIO_NOT_NOTP(1u << pin);
}
