/**
********************************************************************************
* @file			main.h
*
* @brief		Include libraries, contains definitions and macros
*
* @author		Sebastien Masson
*
* @date			July 29th 2018
*
* @version		1.0
********************************************************************************
*/
#include <LPC845.h>
#include "GPIO_LPC845_lib.h"

/**
 * @def BLUELED_PORT
 * LED output port
 * @def BLUELED_PIN
 * LED output pin
 */
#define BLUELED_PORT	1		// P1.15
#define BLUELED_PIN		15

/**
 * @def BUTTON_PORT
 * Button input port
 * @def BUTTON_PIN
 * Button input pin
 */
#define BUTTON_PORT		0		// P0.12
#define BUTTON_PIN		12

/**
 * @def LED_INIT
 * Led initialisation
 * @def BUT_INIT
 * Button initialisation
 */
#define LED_INIT(po,pi)		output_init(po,pi);
#define BUT_INIT(po,pi)		input_init(po,pi);

/**
 * @def KEY_PRESSED
 * Read input status
 */
#define KEYPRESSED(po,pi)	input1_read8(po,pi);

/**
 * @def LED_OFF
 * Turn output on, and the led off (active low)
 */
#define LED_OFF(po,pi)		output1_set1(po,pi);

/**
 * @def LED_ON
 * Turn output off, and the led on (active low)
 */
#define LED_ON(po,pi)		output1_clear1(po,pi);

/**
 * @def LED_TOGGLE
 * Toggles output
 */
#define LED_TOGGLE(po,pi)	output1_toggle1(po,pi);
