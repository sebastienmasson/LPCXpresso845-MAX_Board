/**
 *******************************************************************************
 * @file		main.c
 *
 * @brief		Blinking LED, interrupt driven
 *
 * @author		Sebastien Masson
 *
 * @date		July 28th 2018
 *
 * @version		1.0
 *
 *******************************************************************************
 * @details		Example to turn a LED (active low) on and off using an interrupt
 *				driven algorithm
 *				Target: LPCXpresso845-MAX Board (LPC845)
 *
 * @see			https://www.nxp.com/support/developer-resources/hardware-development-tools/lpcxpresso-boards/lpcxpresso845-max-board-for-lpc84x-mcu-family:OM13097
 *******************************************************************************
 * @mainpage	Project (3): "blinking_led_interrupt_driven"
 * @section 	sec_intro	Description
 *				Example to turn a LED (active low) on and off using an interrupt
 *				driven algorithm
 *				Target: LPCXpresso845-MAX Board (LPC845)
 */
#include <LPC845.h>
#include <assert.h>

/**
 * @def DELAY_CLKTICK
 * SysTick load value - Delay (in CLK ticks) between output state changes
 */
#define DELAY_CLKTICK	0x7FFFFF		// Max reload value = 0x7FFFFF

/**
 * @def GREENLED_PORT
 * Green LED output port
 * @def GREENLED_PIN
 * Green LED output pin
 */
#define GREENLED_PORT	0	// Green LED is P0.0
#define GREENLED_PIN	0

/**
 * @def BLUELED_PORT
 * Green LED output port
 * @def BLUELED_PIN
 * Green LED output pin
 */
#define BLUELED_PORT	1	// Blue LED is P1.15
#define BLUELED_PIN		15

/******************************************************************************/

void SysTick_Handler();							// ISR
void LED_init(uint32_t port, uint32_t pin);
void LED_on(uint32_t port, uint32_t pin);
void LED_off(uint32_t port, uint32_t pin);
void LED_toggle(uint32_t port, uint32_t pin);

/******************************************************************************/

/**
 * @brief	Program entry point
 */
int main()
{
	/* SysTick Initialisation */
	SysTick->CTRL = (	SysTick_CTRL_ENABLE_Msk | 		// Enable SysTick
						SysTick_CTRL_TICKINT_Msk | 		// Enable SysTick IRQ
						SysTick_CTRL_CLKSOURCE_Msk		// CLK = CPU CLK
	);
	SysTick->LOAD = DELAY_CLKTICK;						// Set tick counts
	SysTick->VAL = 0;									// Start value
	
	/* GPIO Initialisation */
	LED_init(GREENLED_PORT, GREENLED_PIN);
	LED_on(GREENLED_PORT, GREENLED_PIN);
	
	LED_init(BLUELED_PORT, BLUELED_PIN);
	LED_off(BLUELED_PORT, BLUELED_PIN);
	
	/* Inifinite loop */
	while(1)
	{
		/* Something else can be done here while the output are toggleing */
	}
}

/******************************************************************************/

/**
 * @brief	SysTick ISR
 */
void SysTick_Handler()
{
	LED_toggle(GREENLED_PORT, GREENLED_PIN);
	LED_toggle(BLUELED_PORT, BLUELED_PIN);
}

/**
 * @brief	Enable the GPIO CLK and set the pin direction
 *
 * @code	LED_init(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_init(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	if (port == 0)
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO0(1);	// CLK GPIO0
	else
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO1(1);	// CLK GPIO1
	
	GPIO->DIR[port] |= GPIO_DIR_DIRP(1u << pin);		// Configure direction
	GPIO->DIRSET[port] |= GPIO_DIRSET_DIRSETP(1u << pin);
}

/**
 * @brief	Switch the LED on (ties it to ground)
 *
 * @code	LED_on(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_on(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->CLR[port] |= GPIO_CLR_CLRP(1u << pin);		// Turn the LED on
}

/**
 * @brief	Switch the LED off (ties it to Vcc)
 *
 * @code	LED_off(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_off(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->SET[port] |= GPIO_SET_SETP(1u << pin);		// Turn the LED off
}

/**
 * @brief	Toggle the LED
 *
 * @code	LED_toggle(1,15);
 * @endcode
 *
 * @param	port	Output port number
 *			pin		Output pin number
 */
void LED_toggle(uint32_t port, uint32_t pin)
{
	assert(port <= 1);
	assert(pin <=32);
	
	GPIO->NOT[port] |= GPIO_NOT_NOTP(1u << pin);		// Toggle the LED
}
